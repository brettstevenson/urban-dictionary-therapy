![UDTherapy](https://user-images.githubusercontent.com/16360374/51782664-0da29500-20e1-11e9-8051-2254b801e21b.png)  
--------------------------------  
#### A simple rehabilitation program for coping with long days of programming  

![Build Status](https://travis-ci.org/tterb/Urban-Dictionary-Therapy.svg?branch=master)
![PyPi Version](https://badge.fury.io/py/UDTherapy.svg)
![Python Version](https://img.shields.io/badge/Python-3-brightgreen.svg)
![License](https://img.shields.io/badge/License-MIT-blue.svg)  
  

![Preview](https://github.com/tterb/Urban-Dictionary-Therapy/blob/master/docs/images/terminal.svg)  


## Description:  
**Urban Dictionary Therapy** is a simple command-line rehabilitation program for coping with long frustrating days of programming. Utilizing this program and the information generously donated by the online community, you too can return to your work as a more successful, functioning member of society. More importantly, Urban Dictionary Therapy provides you with the much needed liberation without the distraction that inevitably comes with opening up a new tab in your browser. Though, this is only one of the many wonderful ways this package can benefit your life.  
  
Maybe you're looking for a substitute for the obligatory ```fortune``` package in your command-line greeting or just simply looking for a way to expand your vocabulary?  
No matter what the issue, ***Urban Dictionary Therapy*** provides a perfect answer to all your woes.  
  
  
## Install  
The program can be installed with the following commands:  

```sh
$ pip install UDTherapy
```
  
## Usage  
The program can be executed as shown:  

```sh
$ UDTherapy [-options]
```
  
#### Options  

| Arguments       |                                                  |
|-----------------|--------------------------------------------------|
| `-s, --search`  |  Display a definition for the specified term     |
| `-n, --num`     |  Specify the number of definitions to display    |
| `-a, --all`     |  Display an entire page of definitions           |
| `-w, -wotd`     |  Display the "*Word of the Day*"                 |
| `-v, --version` |  Display the program version number and exit     |
| `-h, --help`    |  Display information on usage and functionality  |  
  
<br>

## Contributing  
If you'd like to contribute to the project, feel free to suggest a [feature request](./../../issues/new?template=feature_request.md) and/or submit a [pull request](./../../pulls?q=is%3Apr+is%3Aopen+sort%3Aupdated-desc).  
  